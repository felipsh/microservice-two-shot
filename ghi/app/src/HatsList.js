import React, { useEffect, useState } from 'react';

function HatsList({ hats, getHats }) {

    // const [id, setId] = useState('')
    async function handleDelete(id) {
        // setId(event.target.value)
        // let a = event.target.value

        // console.log(typeof a)
        // console.log(typeof id)
        const url = `http://localhost:8090/api/hats/${id}`;
        console.log(url)
        const fetchConfig = {
            method: "DELETE",
        }

        const response = await fetch(url, fetchConfig)
        return getHats()


        // console.log(response)
    }

    // useEffect(() => {
    //     getHats();
    // }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Closet Name</th>
                    <th>Section Number</th>
                    <th>Shelf Number</th>
                    <th></th>
                    {/* <th>Picture</th> */}
                </tr>
            </thead>
            <tbody>
                {hats.map((hat) => {
                    return (
                        <tr key={hat.href}>
                            <td>{hat.name}</td>
                            <td>{hat.fabric}</td>
                            <td>{hat.style}</td>
                            <td>{hat.color}</td>
                            <td>{hat.closet_name}</td>
                            <td>{hat.section_number}</td>
                            <td>{hat.shelf_number}</td>
                            <td>
                                <button className="btn btn-danger" type="button" onClick={() => handleDelete(hat.id)} >Delete</button>
                            </td>
                            {/* <td>
                                <img width="100" src={hat.picture_url} />
                            </td> */}
                        </tr>
                    );
                })}
            </tbody>
        </table>
    )
}

export default HatsList
