import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList'
import HatForm from './HatForm';
import React, { useEffect, useState } from 'react';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';


function App() {
  const [hats, setHats] = useState([]);
  const [shoes, setShoes] = useState([]);
  const getHats = async () => {
    const response = await fetch('http://localhost:8090/api/hats/');
    if (response.ok) {
      const data = await response.json();
      const hats = data.hats;
      setHats(hats);
    } else {
      console.error(response);
    }
  }

  const loadShoes = async () => {
    const response = await fetch('http://localhost:8080/api/shoes/')
    console.log(response);
    if (response.ok) {
      const data = await response.json();
      const shoes = data.shoes;

      setShoes(shoes);

    }
  }

  useEffect(() => {
    getHats();
    loadShoes();
  }, [])


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="new" element={<HatForm getHats={getHats} />} />
            <Route path="" element={<HatsList hats={hats} getHats={getHats} />} />
          </Route>
          <Route path="shoes">
            <Route path="" element={<ShoeList shoes={shoes} loadShoes={loadShoes} />}></Route>
            <Route path="/new" element={<ShoeForm />}></Route>
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );


}

export default App;
