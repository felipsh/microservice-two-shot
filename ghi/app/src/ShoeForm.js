import React, { useEffect, useState } from 'react';

function ShoeForm(props){
    const [bins, setBins] = useState([]);
    const [model_name, setModelName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [bin, setBin] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handleManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePicture = (event) => {
        const value = event.target.value;
        setPicture(value);
    }

    const handleBin = (event) => {
        const value = event.target.value;
        setBin(value);
    }





   const handleSubmit = async (event) => {
        const data = {};
        event.preventDefault();
        data.model_name = model_name;
        data.manufacturer = manufacturer;
        data.color = color;
        data.picture = picture;
        data.bin = bin;


        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
            };
            const response = await fetch(shoesUrl, fetchConfig);
            if (response.ok) {
              const newShoe = await response.json();
              console.log(newShoe);

              setModelName('');
              setManufacturer('');
              setColor('');
              setPicture('');
              setBin('');

            }
          }
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';


        const response = await fetch(url);


        if (response.ok) {
        const data = await response.json();
        console.log("data:", data);
        setBins(data.bins)
        }
        }
                useEffect(() => {
                  fetchData();
                }, []);
                console.log("bins:", bins);

return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a Shoe!</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
                <label htmlFor="model_name" value={model_name}>Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleManufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                <label htmlFor="manufacturer" value={manufacturer}>Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color" value={color}>Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePicture} placeholder="Picture" required type="url" name="picture" id="color" className="form-control" />
                <label htmlFor="picture" value={picture}>Picture</label>
              </div>
              <div className="mb-3">
                <select onChange={handleBin} required type="number" name="bin" id="bin" className="form-select">
                  <option value={bin}>Choose a Bin</option>
                    {bins.map(bin => {
                        console.log("bin:", bin)
                        return (
                            <option key={bin.href} value={bin.id}>
                                {bin.closet_name}
                            </option>
                        )
                    })}
                </select>
              </div>

              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    )
 }
 export default ShoeForm;
