import React, { useEffect, useState } from 'react';

function HatForm() {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [fabric, setFabric] = useState('');
    const [style, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    // const [closetName, setClosetName] = useState('');
    // const [sectionNumber, setSectionNumber] = useState('');
    // const [shelfNumber, setShelfNumber] = useState('');
    const [location, setLocation] = useState('');

    const handleNameChange = event => {
        setName(event.target.value);
    }

    const handleFabricChange = event => {
        setFabric(event.target.value);
    }

    const handleStyleChange = event => {
        setStyle(event.target.value);
    }

    const handleColorChange = event => {
        setColor(event.target.value);
    }

    const handlePictureUrlChange = event => {
        setPictureUrl(event.target.value);
    }

    const handleLocationChange = event => {
        setLocation(event.target.value);
    }
    // const handleClosetNameChange = event => {
    //     setClosetName(event.target.value);
    // }

    // const handleSectionNumberChange = event => {
    //     setSectionNumber(event.target.value);
    // }

    // const handleShelfNumberNumberChange = event => {
    //     setShelfNumber(event.target.value);
    // }

    const handleSubmit = async event => {
        event.preventDefault();
        // console.log(location)
        const data = {};
        data.name = name;
        data.fabric = fabric;
        data.style = style;
        data.color = color;
        data.picture_url = pictureUrl;

        data.location = location;
        const locationUrl = "http://localhost:8090/api/hats/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            setName('');
            setFabric('');
            setStyle('');
            setColor('');
            setPictureUrl('');
            setLocation('');

        }


    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);


        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }

    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Log a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name"
                                className="form-control" value={name} />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFabricChange} type="text" required name="fabric" id="fabric" className="form-control" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStyleChange} type="text" required name="style" id="style" className="form-control" />
                            <label htmlFor="style">Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} type="text" required name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handlePictureUrlChange} type="url" required
                                name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture Url</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} id="location_closet" required name="location" className="form-select">
                                <option value="">Choose a location: (Closet-Section-Shelf)</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.href} value={location.href}>
                                            {location.closet_name} - {location.section_number} - {location.shelf_number}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>

                </div>
            </div>
        </div>
    )
}

export default HatForm;
