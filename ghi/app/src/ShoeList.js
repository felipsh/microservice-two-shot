

function ShoeList({shoes, loadShoes}){
    const DeleteShoe = async (id) => {
        const response = await fetch(`http://localhost:8080/api/shoes/${id}`, {
            method: "DELETE",

        })
        return loadShoes();

    };
    if (shoes === undefined) {
        return null;
    }

        console.log("shoes:", shoes);

    return(
<>
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Model Name</th>
            <th>Manufacturer</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Closet Name</th>
            <th>Bin Number</th>
            <th>Bin Size</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map(shoe => {
            return (
              <tr key={shoe.href}>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.picture }</td>
                <td>{ shoe.closet_name }</td>
                <td>{ shoe.bin_number }</td>
                <td>{ shoe.bin_size }</td>
                <td>
                    <button type="button" className="btn btn-warning" onClick={() => DeleteShoe(shoe.id)}>Delete</button>
                </td>
              </tr>
            );
          })}

        </tbody>
      </table>
</>
    )
}
export default ShoeList;
