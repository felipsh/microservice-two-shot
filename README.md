# Wardrobify

Team: 30

* Michelle Xue - Hats
* Tyler Herman - Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.
In my models I created a shoe model with a foreign key to refer to the bin as a value object as a separate model. That allowed us to communicate to the wardrobe database, allowing us to create bins and then shoes in them to serve as a database and supply information for the wardrobify application.

## Hats microservice

I created a Hat model including properties of name, fabric, style, color, and picture_url. Then I created a model for location as a value object because this model will be used to create location instances from data polled from Wardrobe Api. The hat model will use this as a foreign key.

I
