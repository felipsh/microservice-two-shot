from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from django.shortcuts import render

from common.json import ModelEncoder
from .models import Hat, LocationVO
# Create your views here.

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "section_number", "shelf_number", "import_href"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "name",
        "fabric",
        "style",
        "color",
        "picture_url",
        # "location"
        ]
    def get_extra_data(self, o):
        return {
            "closet_name": o.location.closet_name,
            "section_number": o.location.section_number,
            "shelf_number": o.location.shelf_number,
        }

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "fabric",
        "style",
        "color",
        "picture_url",
        "location"
    ]

    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location_href= content["location"]
            # print("content before reassign", content)
            # print("location href", location_href)
            # testlocations=LocationVO.objects.all()
            # for testlocation in testlocations:
            #     print("each location", testlocation)
            #     print(testlocation.import_href)

            location = LocationVO.objects.get(import_href=location_href)
            # print("?",new_location)
            content["location"] = location
            # print("content after reassign", content)
        except LocationVO.DoesNotExist:
            # print("location href", location_href)

            return JsonResponse(
                {"message": "Invald location id"}, status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
