from django.db import models


# Create your models here.

class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, default=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveIntegerField(default=True)



class Shoe(models.Model):
    manufacturer = models.CharField(max_length=50)
    model_name = models.CharField(max_length=50)
    color = models.CharField(max_length=20)
    picture = models.URLField(max_length=200, null=True)
    bin = models.ForeignKey(
        "BinVO",
        related_name="shoe",
        on_delete=models.CASCADE,
    )
